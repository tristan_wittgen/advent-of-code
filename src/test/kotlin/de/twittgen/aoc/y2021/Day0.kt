package de.twittgen.aoc.y2021

import de.twittgen.aoc.y2019.shared.util.FileUtil
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class Day0 {
    val input by lazy { FileUtil.readInput("2021/day0").parse() }
    val example = """""".parse()

    private fun String.parse() = lines()


    @Test
    fun example() {

    }

    @Test
    fun example2() {

    }

    @Test
    fun part1() {

    }

    @Test
    fun part2() {

    }
}

